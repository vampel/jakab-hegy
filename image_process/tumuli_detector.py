import cv2 as cv
import numpy as np
import shape_detector as sd
import math

MINMAX_THERSH=0
OSTU_THRESH = 1
MEAN_THRESH = 2
MEDIAN_THRESH=3
MULTI_THRESH= 4

def oilWaterFlood(data, radius, oilthickness, uncer=0.25, start_height=None, end_height=None, circle_threshold=0.75, c1=False):
    if start_height is None or end_height is None:
        start_height, end_height, ret, ret = cv.minMaxLoc(data)
        start_height = math.floor(start_height)
        end_height = math.ceil(end_height)
    d_rad = round(uncer*radius)
    count = 0
    mask = np.zeros(data.shape[:2], np.uint8)
    detect = list()
    for oil in range(start_height, end_height, oilthickness):
        if count > 10:
            break
       
        if np.sum((data >= oil)*(data < oil+oilthickness)) == 0:
            count += 1
            continue
        else:
            count = 0

        oilrise = np.zeros(data.shape[:2], np.uint8)
        oilrise[(data >= oil)] = 255

        rects = sd.rectangle(oilrise, radius-d_rad, radius+d_rad)
        
        for rect in rects:
            cv.drawContours(mask, [rect], 0, 255, -1)
        
        if c1:
            flooded = np.ones(data.shape[:2], np.int)*-1
            flooded[(data >= oil)*(data < oil+oilthickness)] = 1

            circles = sd.convolutionCircle(flooded, radius-d_rad, radius+d_rad, circle_threshold)
        else:
            flooded = np.zeros(data.shape[:2], np.uint8)

            flooded[(data >= oil)*(data < oil+oilthickness)] = 255
            circles = sd.HoughCircle(flooded ,radius-d_rad, radius+d_rad)

        if len(circles) != 0:
            for param in circles[0]:
                x, y = param[:2]

                if mask[y,x] != 255:
                    detect.append( param )
    return detect

def oilWaterFloodExtended(data, radius,divider, strech=4, uncer=0.25,threshold=0.75, checker=None, c1=False):
    Rows, Cols = data.shape[:2]
    detect = list()
    step = math.ceil(min(Rows,Cols)/divider)
    over_lap = math.ceil(radius*(1+uncer))
    
    for r in range(0,Rows,step):
        for c in range(0, Cols, step):
            stR = max(0, r-over_lap)
            edR = min(Rows, r+step+over_lap)
            stC = max(0, c-over_lap)
            edC = min(Cols, c+step+over_lap)
            temp = histogram_strech(data[stR:edR, stC:edC], strech)
            params = oilWaterFlood(temp,radius,1,uncer=uncer, circle_threshold=threshold,c1=c1)
            for param in params:
                col, row, rad = param
                if checker != None and row >= rad and row < Rows-rad and col >= rad and col < Cols-rad: 
                    temp = data[row-rad:row+rad+1,col-rad:col+rad+1]
                    if tumuli_checker(temp, checker):
                        detect.append([col+stC, row+stR, rad])
                else:
                    detect.append([col+stC, row+stR, rad])

    return detect

def tumuli2(data, radius, uncer, circle_threshold, threshold_type=0,c1=False):
    Rows, Cols = data.shape[:2]
    detect = list()
    radMin = math.floor(radius*(1-uncer))
    radMax = math.ceil(radius*(1+uncer))
    step = radMax
    mask = np.zeros( (Rows, Cols), np.uint8)
    thresh = list()
    for shift_r in [0, step]:
        for shift_c in [0, step]:
            threshold_data = np.zeros(data.shape[:2]) 
            for r in range(shift_r, Rows,step*2):
                for c in range(shift_c, Cols,step*2):
                    re = min(r+step*2,Rows)
                    ce = min(c+step*2,Cols)
                    temp = data[r:re,c:ce]
                    tempT = np.zeros(temp.shape)
                    minV, maxV = cv.minMaxLoc(temp)[:2]

                    if threshold_type == MINMAX_THERSH:
                        threshold = (minV+maxV)/2 
                    elif threshold_type == OSTU_THRESH:
                        threshold = getOtsuThreshold(temp)
                    elif threshold_type == MEAN_THRESH:
                        threshold = getMeanThreshold(temp)
                    elif threshold_type == MEDIAN_THRESH:
                        threshold = getMedianThreshold(temp)
                    elif threshold_type == MULTI_THRESH:
                        th_step = (maxV - minV)/5
                        for i in range(1,6):
                            threshold = minV+th_step*i
                            tempT[temp >= threshold] += 1
                        threshold_data[r:re,c:ce] = tempT
                        continue                    
                    else:
                        print("\033[91mThreshold type not exested!\033[0m")
                        return []
                    
                    tempT[temp >= threshold] = 1
                    threshold_data[r:re,c:ce] = tempT
            thresh.append(threshold_data)
            if threshold_type == MULTI_THRESH:
                circles = list()
                for i in range(5,0,-1):
                    tempT = np.zeros(threshold_data.shape)
                    tempT[threshold_data == i] = 1
                    if c1:
                        tempT = tempT*2-1
                        circle = sd.convolutionCircle(tempT,radMin, radMax,circle_threshold, pad=1)
                    else:
                        tempT = np.zeros(threshold_data.shape, np.uint8)
                        tempT[threshold_data > 0] = 255
                        circle = sd.HoughCircle(tempT ,radMin, radMax)

                    for c in circle:
                        circles.append(c)
            else:
                if c1:
                    threshold_data = threshold_data*2 -1
                    circles = sd.convolutionCircle(threshold_data,radMin, radMax,circle_threshold)
                else:
                    tempT = np.zeros(threshold_data.shape, np.uint8)
                    circles = sd.HoughCircle(tempT ,radMin, radMax)

            if len(circles) != 0:
                for x,y,rad in circles[0]:
                    if mask[y,x] != 0:
                        continue
                    detect.append([x, y, rad])
                    cv.circle(mask, (x,y), rad,1,-1)
    return (detect, thresh)

def tumuli(data, radius, uncer, ksize, circle_threshold, c1=False):
    Rows, Cols = data.shape[:2]
    detect = list()
    radMin = math.floor(radius*(1-uncer))
    radMax = math.ceil(radius*(1+uncer))
    
    local_max = list()
    for r in range(0,Rows, ksize):
        for c in range(0, Cols, ksize):
            temp = data[r:r+ksize+1, c:c+ksize+1]
            minV, maxV, _, loc = cv.minMaxLoc(temp)
            while maxV > 9000:
                y, x = loc 
                temp[x,y] = minV
                _, maxV, _, loc = cv.minMaxLoc(temp)
            y, x = loc
            local_max.append( (x+r,y+c) )

    for r,c in local_max:
        sR = max(0, r-ksize)
        sC = max(0, c-ksize)
        eR = min(Rows, r+1+ksize)
        eC = min(Cols, c+1+ksize)
        temp = data[sR:eR,sC:eC]
        minV, maxV, _, loc = cv.minMaxLoc( temp )
        while maxV > 9000:
            y,x = loc
            temp[x,y] = minV
            minV, maxV, _, _ = cv.minMaxLoc( temp )
        
        dist = (maxV - minV)/5
        for i in range(4,0,-1):
            tempT = temp > minV + dist *i
            if c1:
                tempT = tempT*2 -1
                circle = sd.convolutionCircle(tempT,radMin, radMax,circle_threshold)
            else:
                tempTI = np.zeros(tempT.shape, np.uint8) 
                tempTI[tempT] = 255
                circle = sd.HoughCircle(tempTI ,radMin, radMax)
            
            if len(circle[0]) != 0:
                for retC, retR, rad in circle[0]:
                    detect.append( [retC + sC, retR + sR, rad] )
                break

    return detect

def histogram_strech(data, strech=None):
    minV, maxV = cv.minMaxLoc(data)[:2]
    if strech == None or strech <= 0:
        data = ((data-minV)/(maxV-minV))*255
    else:
        data = (data-minV)*strech

    return data

def tumuli_checker(data, uncer):
    Rows, Cols = data.shape[:2]
    if Rows != Cols:
        print("Not square matrix! " + repr(Rows) + " " + repr(Cols))
        return None
    rad = math.floor(Rows/2)
    V = list()
    for s in range(rad+1):
        V.append(np.sum(data[rad-s:rad+s+1,rad-s:rad+s+1])/(s*2+1)**2)
    
    thres = len(V)*uncer
    decrease = 0
    prev = V[0]
    for v in V:
        if prev < v:
            decrease += 1
            if thres < decrease:
                return False
        prev = v
    return True

def getOtsuThreshold(data):
    R, C = data.shape[:2]
    hist, border = np.histogram(data,R*C)
    hist = hist/R*C
    Q = hist.cumsum()
    bins = np.arange( len(hist) )
    thres = border[0]
    fn_min = np.inf
    for i in range(1, R*C-1):
        p1,p2 = np.hsplit(hist,[i])
        q1,q2 = Q[i], Q[-1]-Q[i]
        b1, b2 = np.hsplit(bins,[i])

        m1,m2 = np.sum(p1*b1)/(max(q1,1)), np.sum(p2*b2)/ (max(q2,1))
        v1,v2 = np.sum( ((b1-m1)**2)*p1 )/(max(q1,1)), np.sum( ((b2-m2)**2)*p2 )/ ( max(q2,1) )
        fn = v1*(max(q1,1)) +v2*(max(q2,1))
        if fn < fn_min:
            fn_min = fn
            thres = border[i]
    return thres

def getMeanThreshold(data):
    return  np.mean(data)

def getMedianThreshold(data):
    return np.median(data)