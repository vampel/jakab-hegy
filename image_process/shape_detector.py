import cv2 as cv
import numpy as np
import math

COMPRESS_NONE = cv.CHAIN_APPROX_NONE
COMPRESS_SMALL = cv.CHAIN_APPROX_SIMPLE
COMPRESS_LARGE = cv.CHAIN_APPROX_TC89_L1

def binaryHoughCircle(binary, mode, min_rad=0, max_rad=0, threshold=0.85, ksize=3):
    R, C = binary.shape[:2]
    
    if min_rad <= 0:
        rad_shift = 1
    else:
        rad_shift = min_rad

    if max_rad <= 0 or max_rad <= min_rad:    
        rad_end = math.floor(min(R,C)/2) - rad_shift + 1
    else:
        rad_end = max_rad+1 - rad_shift
    
    if round(R/ksize) == math.ceil(R/ksize):
        R = math.ceil(R/ksize)
    else:
        R = math.floor(R/ksize)
    if round(C/ksize) == math.ceil(C/ksize):
        C = math.ceil(C/ksize)
    else:
        C = math.floor(C/ksize)

    ret = list()
    _, contour, _ = cv.findContours(binary, cv.RETR_TREE, mode)
    for cont in contour:
        acc = np.zeros((R, C,rad_end), np.int)
        thres = len(cont)*threshold
        for c in cont:
            x, y = c[0]
            for rad in range(rad_end):
                vote_points = list()        
                for tau in range(360):
                    a = int(round( (y - (rad + rad_shift)* math.cos(tau*np.pi/180))/ksize ))
                    b = int(round( (x - (rad + rad_shift)* math.sin(tau*np.pi/180))/ksize ))
                    if a < R and b < C and a > 0 and b > 0 and (a, b) not in vote_points:
                        vote_points.append((a,b))
                        
                        acc[a,b, rad] += 1
            vote = np.max(acc)
            if vote >= thres:
                A,B,Rad = np.where(acc >= vote)
                for ind in range(len(A)):
                    a = A[ind]
                    b = B[ind]
                    rad = Rad[ind]
                    ret.append([b*ksize,a*ksize,rad+rad_shift])
    return [ret]

def HoughCircle(binary, min_rad=0, max_rad=0):
    _, contour, _ = cv.findContours(binary, 1, 2)
    R,C = binary.shape

    if min_rad <= 0:
        rad_shift = 1
    else:
        rad_shift = min_rad

    if max_rad <= 0 or max_rad <= min_rad:    
        rad_end = math.floor(min(R,C)/2) - rad_shift + 1
    else:
        rad_end = max_rad+1 - rad_shift

    acc = np.zeros((R, C, rad_end), np.int)

    ret = list()
    for cnt in contour:
        curve = 0.01*cv.arcLength(cnt,True)
        approx = cv.approxPolyDP(cnt, curve, True)
        nOP = len(approx)
        if 16 >= nOP >= 10:
            for point in approx:
                x, y = point[0]
                for rad in range(rad_end):
                    vote_points = list()
                    for tau in range(360):
                        a = int(round( y - (rad + rad_shift)* math.cos(tau*np.pi/180) ) )
                        b = int(round( x - (rad + rad_shift)* math.sin(tau*np.pi/180) ) )
                        if R > a >= 0 and C > b >= 0 and binary[a,b] == 255 and (a, b) not in vote_points:
                            vote_points.append( (a,b) )
                            acc[a,b, rad] += 1
            vote = np.max(acc)
            if vote >= nOP-5:
                A,B,Rad = np.where(acc >= vote)
                for ind in range(len(A)):
                    a = A[ind]
                    b = B[ind]
                    rad = Rad[ind]
                    ret.append([b,a,rad+rad_shift])
    return [ret]     

def convolutionCircle(binary, min_rad, max_rad, percent=1, pad=1):
    kernels = list()
    for rad in range(max_rad, min_rad-1, -1):
        ksize = 2*(rad+pad)+1
        if ksize < 3:
            break
        kernel = np.ones((ksize, ksize))*-1
        cv.circle(kernel, (rad+pad,rad+pad), rad, 1, -1)
        kernels.append((kernel, rad))
    
    ret = list()

    mask = np.zeros(binary.shape, np.uint8)
    for kernel, rad in kernels:
        thres = int((kernel.shape[0]**2)*percent)
        conv = cv.filter2D(binary,-1, kernel)
        
        
        A, B = np.where(conv >= thres)

        for ind in range(len(A)):
            a = A[ind]
            b = B[ind]
            if mask[a, b] == 0:
                ret.append([b, a, rad])
                cv.circle(mask, (b,a), rad, 255)

    return [ret]

def rectangle(binary, min_a, max_a, percent=0.01):
    R, C = binary.shape
    _, cont, _ = cv.findContours(binary, 1, 2)
    ret = list()
    for cnt in cont:
        curve = percent*cv.arcLength(cnt,True)
        approx = cv.approxPolyDP(cnt, curve, True)
        if 4 <= len(approx) <= 10:
            if  [[0, R-1]] not in approx and [[C-1, 0]] not in approx and [[C-1, R-1]] not in approx and [[0, 0]] not in approx:            
                tmp = np.zeros((R,C),np.uint8)
                cv.drawContours(tmp,[approx],0, 1,-1)
                if (2*min_a+1)**2 <= np.sum(tmp) <= (2*max_a+1)**2:
                    ret.append(approx)

    
    return ret