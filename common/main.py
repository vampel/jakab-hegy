import numpy as np
import cv2 as cv
import sys
sys.path.append("../image_process/")
import tumuli_detector as td
import shape_detector as sd
import time as t

def read_grd(filename):
	data = list()
	with open(filename) as o_file:
		code = o_file.readline().split()[0]
		aux = o_file.readline().split()
		nx = int(aux[0])
		ny = int(aux[1])
		aux = o_file.readline().split()
		xmin = float(aux[0])
		xmax = float(aux[1])
		aux = o_file.readline().split()
		ymin = float(aux[0])
		ymax = float(aux[1])
		aux = o_file.readline().split()
		zmin = float(aux[0])
		zmax = float(aux[1])
		for line in o_file:
			row = line.split()
			row = list(map(float, row))
			data.append(row)
	param = (code, (nx, ny), (xmin, xmax), (ymin, ymax), (zmin, zmax))
	return (param, np.array(data))

def data_to_image(data):
	import math
	
	minV, maxV, ret, ret = cv.minMaxLoc(data)
	minV = math.floor(minV)
	maxV = math.ceil(maxV)
	Color = [0, 255, 0]
	Color_ind = 2
	Color_inc = 1
	R,C = data.shape[:2]
	ret = np.zeros((R,C,3), np.uint8)
	count = 0
	for height in range(minV, maxV):
		hmap = np.zeros((R,C), np.uint8)
		hmap[(data >= height)* (data < height+1)] = 255

		if np.sum(hmap) == 0:
			count += 1
		else:
			count = 0
		r, contour, hier = cv.findContours(hmap, cv.RETR_TREE, cv.CHAIN_APPROX_NONE)
		
		Color[Color_ind] += Color_inc
		if Color_ind == 2 and Color in [[0,255,100], [0,150,255]]:
			Color_ind = 1
			Color_inc = -1
		elif Color_ind == 2 and Color == [0,150,100]:
			Color_ind = 2
			Color_inc = 1
		ret[hmap == 255] = Color
		for cont in contour:
			cv.drawContours(ret, [cont], 0, (0,0,0))
		if count >= 100:
			break	
	return ret

def draw_detected(image, params, color=(0,0,255), line=2, mode='circle'):
	if mode == 'circle':
		for x, y, rad in params:
			cv.circle(image, (x,y), rad, color, line)
	elif mode == 'rect':
		for p1, p2 in params:
			cv.rectangle(image, p1, p2, color, line)

def count_detected(data, image, params):
	sure_height = list()
	maybe_height = list()
	else_height = list()
	R, C = image.shape[:2]
	mask = np.zeros((R,C), np.uint8)
	for x, y, rad in params:
		if not( R > y >= 0 ) or not ( C > x >= 0) or mask[y,x] != 0:
			continue
		minV, maxV, minL, maxL = cv.minMaxLoc( data[y-rad:y+rad+1,x-rad:x+rad+1] )
		height = maxV - minV
		if image[y,x,2] == 255:
			sure_height.append( (height, minL, maxL) )
		elif image[y,x,0] == 255:
			maybe_height.append( (height, minL, maxL) )
		else:
			else_height.append( (height, minL, maxL) )
		cv.circle(mask, (x,y), rad, 255,-1)
	return (sure_height, maybe_height, else_height)

def print_result(data,tags, params, txtFile=None):
	tmp = tags[:,:,0]
	_, labels = cv.connectedComponents(tmp)
	mm = np.max(labels)
	tmp = tags[:,:,2]
	_, labels = cv.connectedComponents(tmp)
	ms = np.max(labels)
	sL, mL, eL = count_detected(data,tags, ret)
	
	s = len(sL)
	m = len(mL)
	e = len(eL)

	if txtFile is None:
		print( "Sure tumulis: %s piece of %s piece." % (s, ms))
		print( "Maybe tumulis: %s piece of %s piece." % (m, mm))
		print( "Not tumulis: %s piece." % (e) )
	else:
		txtFile.write( "\tSure  tumulis: %s piece of %s piece.\r\n" % (s, ms))
		cnt = 0
		txtFile.write("\t\t")
		for h, minL, maxL in sL:
			txtFile.write("D: %s; minL: %s; maxL: %s" % (h,minL, maxL) )
			txtFile.write("\r\n\t\t")
		txtFile.write("\r\n")		
		txtFile.write( "\tMaybe tumulis: %s piece of %s piece.\r\n" % (m, mm))
		cnt = 0
		txtFile.write("\t\t")
		for hh, minL, maxL in mL:
			txtFile.write("D: %s; minL: %s; maxL: %s" % (h,minL, maxL) )
			txtFile.write("\r\n\t\t")
		txtFile.write("\r\n")
		txtFile.write( "\t Not  tumulis: %s piece.\r\n" % (e) )
		cnt = 0
		txtFile.write("\t\t")
		for h, minL, maxL in eL:
			txtFile.write("D: %s; minL: %s; maxL: %s" % (h,minL, maxL) )
			txtFile.write("\r\n\t\t")
		txtFile.write("\r\n")		

def print_time(name, start, end, txtFile=None):
	time_in_sec = end -start
	hour =  int(time_in_sec/3600)
	minu = int(time_in_sec / 60) - hour*60
	sec = int(time_in_sec) - hour*3600 - minu*60
	if txtFile is None:
		print(name + ": " + repr(hour) + " h " + repr(minu) + " m " + repr(sec) + " sec")
	else:
		txtFile.write( "\t%s: %s h %s m %s sec\r\n" % (name, hour, minu, sec))

def print_tag_height(data, tags, txtFile):
	tags = cv.cvtColor(tags, cv.COLOR_BGR2GRAY)
	_, labels = cv.connectedComponents(tags)
	mV = np.max(labels)

	for l in range(1,mV):
		temp = np.zeros(data.shape, np.float)
		temp[labels==l] = data[labels == l]
		_, maxV, _, maxL = cv.minMaxLoc(temp)
		temp = np.ones(data.shape, np.float)*np.inf
		temp[labels==l] = data[labels == l]
		minV, _, minL, _ = cv.minMaxLoc(temp)
		diff = maxV - minV
		txtFile.write("Diff: %s; minloc: %s; maxloc %s\r\n" % (diff, minL, maxL))

if __name__ == "__main__":
	func = sys.argv[1]

	if func == 'data':
		full = sys.argv[2]
		alg = sys.argv[3]
		filt = sys.argv[4]
		circle = sys.argv[5]
		threshold = float(sys.argv[6])
		div = int(sys.argv[7])
		strech = int(sys.argv[8])
		
		if len(sys.argv) == 10:
			chck = float(sys.argv[9])
		else:
			chck = None

		rad = 10
		uncer = 0.25
		if filt == 'filter':
			filt = True
		else:
			filt = False

		if circle == 'hough':
			circle = False
		else:
			circle = True

		s = t.time()

		param, data = read_grd("../data/JAKAB_ASCII.grd")
		
		e = t.time()
		print_time("Read in", s, e)

		if filt:
			data_filtered = cv.medianBlur(cv.copyMakeBorder(data, 5,5,5,5,cv.BORDER_REFLECT_101).astype(np.float32),5)
		else:
			data_filtered = data
		

		if full != 'full':
			data_filtered = data_filtered[4200:5000,400:1200]
			tags = tags[4200:5000, 400:1200,:]


		s = t.time()
		print_time("Filter", e, s)
		
		ret = []
		ll = []
		if alg == "owf":
			ret = td.oilWaterFlood(data_filtered, rad, 1, uncer=uncer, circle_threshold=threshold,c1=circle)
		elif alg == "extend":
			ret = td.oilWaterFloodExtended(data_filtered, rad, div, strech=strech, threshold=threshold, checker=chck,c1=circle)
		elif alg == "tumuli2_0":
			ret, ll = td.tumuli2(data_filtered, rad,uncer,circle_threshold=threshold,threshold_type=td.MINMAX_THERSH, c1=circle)
		elif alg == "tumuli2_1":
			ret, ll = td.tumuli2(data_filtered, rad,uncer,circle_threshold=threshold,threshold_type=td.OSTU_THRESH, c1=circle)
		elif alg == "tumuli2_2":
			ret , ll= td.tumuli2(data_filtered, rad,uncer,circle_threshold=threshold,threshold_type=td.MEAN_THRESH, c1=circle)
		elif alg == "tumuli2_3":
			ret , ll= td.tumuli2(data_filtered, rad,uncer,circle_threshold=threshold,threshold_type=td.MEDIAN_THRESH, c1=circle)
		elif alg == "tumuli2_4":
			ret , ll= td.tumuli2(data_filtered, rad,uncer,circle_threshold=threshold,threshold_type=td.MULTI_THRESH, c1=circle)
		elif alg == "tumuli":
			ret = td.tumuli(data_filtered, rad,uncer,20,circle_threshold=threshold,c1=circle)
		else:
			alg = "data"

		e = t.time()
		print_time("Detect", s, e)

		image = data_to_image(data_filtered)

		s = t.time()
		print_time("Convert", e, s)

		draw_detected(image, ret, color=(255,0,0))

		e = t.time()
		print_time("Draw param", s, e)

		image = np.flip(image, 0)
		cv.imwrite("../../../image_" + alg + ".jpg", image)


		print_result(data,tags, ret)
		

	elif func=='data_all':
		full = sys.argv[2]
		count = int(sys.argv[3])
		filt = sys.argv[4]
		circle = sys.argv[5]
		threshold = float(sys.argv[6])
		div = int(sys.argv[7])
		strech = int(sys.argv[8])
		
		if len(sys.argv) == 10:
			chck = float(sys.argv[9])
		else:
			chck = None

		rad = 10
		uncer = 0.25
		if filt == 'filter':
			filt = True
		else:
			filt = False

		if circle == 'hough':
			circle = False
		else:
			circle = True

		path = "../../../test_all/" + repr(count) + "/"
		log = open("../../../test_all/log_" + repr(count) + ".txt", "w")
		#log.write("Alg: %s; filter: %s; hough: %s; threshold: %s; div#: %s; strech: %s; checker: %s\r\n" %(alg,filt,circle,threshold,div,strech,chck) )
		log.write("Filter: %s; hough: %s; threshold: %s; div#: %s; strech: %s; checker: %s\r\n" %(filt,not circle,threshold,div,strech,chck) )
		s = t.time()

		param, data = read_grd("../data/JAKAB_ASCII.grd")
		tags = cv.imread("../data/jakab_tag_tumuli.tif")
		tags = np.flip(tags, 0)

		e = t.time()
		print_time("Read in", s, e, log)

		if filt:
			data_filtered = cv.medianBlur(cv.copyMakeBorder(data, 5,5,5,5,cv.BORDER_REFLECT_101).astype(np.float32),5)
		else:
			data_filtered = data
		

		if full != 'full':
			data_filtered = data_filtered[4200:5000,400:1200]
			tags = tags[4200:5000, 400:1200,:]

		s = t.time()
		print_time("Filter", e, s, log)

		image = data_to_image(data_filtered)

		e = t.time()
		print_time("Convert", s, e, log)

		Rows, Cols = data_filtered.shape
		
		for alg in ['tumuli']: #['owf','extend','tumuli2_0','tumuli2_1', 'tumuli2_2', 'tumuli2_3', 'tumuli']:
			log.write("Algorithm: %s\r\n" % alg)

			s = t.time()
			ret = []
			ll = []
			if alg == "owf":
				ret = td.oilWaterFlood(data_filtered, rad, 1, uncer=uncer, circle_threshold=threshold,c1=circle)
			elif alg == "extend":
				ret = td.oilWaterFloodExtended(data_filtered, rad, div, strech=strech, threshold=threshold, checker=chck,c1=circle)
			elif alg == "tumuli2_0":
				ret, ll = td.tumuli2(data_filtered, rad,uncer,circle_threshold=threshold,threshold_type=td.MINMAX_THERSH, c1=circle)
			elif alg == "tumuli2_1":
				ret, ll = td.tumuli2(data_filtered, rad,uncer,circle_threshold=threshold,threshold_type=td.OSTU_THRESH, c1=circle)
			elif alg == "tumuli2_2":
				ret , ll= td.tumuli2(data_filtered, rad,uncer,circle_threshold=threshold,threshold_type=td.MEAN_THRESH, c1=circle)
			elif alg == "tumuli2_3":
				ret , ll= td.tumuli2(data_filtered, rad,uncer,circle_threshold=threshold,threshold_type=td.MEDIAN_THRESH, c1=circle)
			elif alg == "tumuli2_4":
				ret , ll= td.tumuli2(data_filtered, rad,uncer,circle_threshold=threshold,threshold_type=td.MULTI_THRESH, c1=circle)
			elif alg == "tumuli":
				ret = td.tumuli(data_filtered, rad,uncer,20,circle_threshold=threshold,c1=circle)
			else:
				alg = "data"

			e = t.time()
			print_time("Detect", s, e, log)

			det_img = np.zeros( (Rows, Cols, 3), np.uint8 )
			draw_detected(det_img, ret, color=(255,0,0))
			det_img = np.flip(det_img,0)
			cv.imwrite(path + alg + ".jpg", det_img)
			for num in range(len(ll)):
				temp = np.zeros(ll[0].shape, np.uint8)
				temp[ll[num] == 1] = 255
				cv.imwrite(path + alg +"_" + repr(num) + ".jpg", temp)

			s = t.time()
			print_time("Draw param", e, s, log)

			print_result( data_filtered,tags, ret, log)
			log.write("End\r\n")

		log.close()
		#image = np.flip(image, 0)
		#cv.imwrite(path + "map.jpg", image)
		#cv.imwrite(path + "pic.jpg", pic)
	elif func == 'draw':
		path = sys.argv[2]
		small = sys.argv[3]
		if small == '0':
			small = False
		else:
			small = True
		fileNs = ["extend.jpg", "owf.jpg", "tumuli.jpg", "tumuli2_0.jpg", "tumuli2_1.jpg", "tumuli2_2.jpg", "tumuli2_3.jpg"]

		pic1 = cv.imread("../data/jakab.tif")
		pic2 = cv.imread("../data/image_data.jpg")
		if small:
			pic1 = np.flip(pic1, 0)
			rows = pic1.shape[0]
			pic1 = pic1[rows-7500:rows, 0:5003,:]
			pic2 = np.flip(pic2, 0)
			rows = pic2.shape[0]
			pic2 = pic2[rows-7500:rows, 0:5003,:]
		else:
			pic1 = pic1[0:7500,0:5003,:] 
			pic2 = pic2[0:7500,0:5003,:]
		for fileN in fileNs:
			fN = fileN.split('.')[0]
			result = cv.imread(path+fileN)
			if result is None:
				continue
			if small:
				gr = np.flip(result,0)[:,:,0]
				
			else:
				gr = result[0:7500,0:5003,0]
			for p, pN, color in [(pic1, 'gray_',[0,0,255]), (pic2, 'map_',[255,0,0])]:
				temp = p.copy()
				if small:
					tmp = temp[4200:5000,400:1200]
					tmp[gr >= 100] = color
					temp = np.flip(temp, 0)
					tmp = np.flip(tmp,0)
					rrr = [ (temp[2500:5000,:,:], '0'), (tmp, '1')]
				else:
					tmp = np.zeros(temp.shape[:2], np.uint8)
					tmp[gr >= 100] = 255
					_, cont, _ = cv.findContours(tmp, cv.RETR_TREE,cv.CHAIN_APPROX_SIMPLE)
					for cnt in cont:
						cv.drawContours(temp,[cnt],0,color,-1)

					tmp = np.flip(temp,0)
					tmp =  tmp[4200:5000,400:1200,:]
					tmp = np.flip(tmp, 0)
					rrr = [(temp, '0'),(tmp, '1')]
				for tmp, n in rrr:
					cv.imwrite(path + "draw_" + pN + fN + "_" + n + ".jpg", tmp)

	print("FINISH")
	cv.waitKey(0)